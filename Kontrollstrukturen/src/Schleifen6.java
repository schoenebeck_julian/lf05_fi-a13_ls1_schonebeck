import java.util.Scanner;

public class Schleifen6 {
	public static void main(String [] args) {
		Scanner tastatur = new Scanner(System.in);
		char abfrage='j';
		while (abfrage!='n') {
			System.out.print("Geben Sie bitte Ihre einmalige Einlage ein:");
			double einlage = tastatur.nextDouble();
			System.out.print("Geben Sie bitte den Zinssatz ein:");
			double zins = tastatur.nextDouble();
			double jahresplus=0;
			int anzahl=0;
			if (einlage!=1000000) {
				for (int monatsplus=0; monatsplus<13; monatsplus++) {
					jahresplus=einlage*(zins/100);
					einlage=einlage+(einlage*(zins/100));
					anzahl++;
				}
			}
			System.out.println("Das ist Ihr Jahresprofit:"+jahresplus);
			System.out.println("Sie brauchen "+anzahl+" Jahre!");
			System.out.println("Wenn Sie eine weitere Rechnung durchführen wollen, dann tippen Sie bitte j und wenn nicht dann n: ");
			abfrage=tastatur.next().charAt(0);
		}
	}
}
