import java.util.Scanner;

public class Schleifen1 {
	public static void main(String [] args) {
		Scanner tastatur = new Scanner(System.in);
		int n = tastatur.nextInt();
		for(int i = 1; i < n; i++){
			System.out.println(i);
		}
	}
}
