import java.util.Scanner;
public class Auswahlstrukturen5 {
	public static void main(String [] args) {
		Scanner tastatur = new Scanner(System.in);
		int gewicht = tastatur.nextInt();
		double groesse = tastatur.nextDouble();
		boolean mann = tastatur.nextBoolean();
		groesse= groesse*groesse;
		double bmi = gewicht / groesse;
		if (mann == true){
			if (bmi<20){
				System.out.println(bmi);
			}
			else if (bmi<26 && bmi>19) {
				System.out.println("Normalgewicht");
			}
			else if (bmi>25) {
				System.out.println("Übergewicht");
			}
		}
		else {
			if (bmi<19){
				System.out.println("Untergewichtig");
			}
			else if (bmi<25 && bmi>18) {
				System.out.println("Normalgewicht");
			}
			else if (bmi>24) {
				System.out.println("Übergewicht");
			}
		}
	}
}
