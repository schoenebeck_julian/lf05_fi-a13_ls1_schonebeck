import java.util.Scanner;
public class Schleifen3 {
	public static void main(String [] args) {
		Scanner tastatur = new Scanner(System.in);
		for(int i = 1; i <= 200; i++) {
			if(i%7==0) {
				System.out.println("Die Zahl "+i+" ist durch 7 teilbar");
			}
			else {
				System.out.println("Die Zahl "+i+" ist nicht durch 7 teilbar");
			}
		}
	}
}
