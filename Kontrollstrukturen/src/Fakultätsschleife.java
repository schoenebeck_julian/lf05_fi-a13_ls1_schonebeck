import java.util.Scanner;
public class Fakultätsschleife {
	public static void main(String [] args) {	
		Scanner tastatur = new Scanner(System.in);
		int n = tastatur.nextInt();
		System.out.println(fakultaet(n));
	}


	public static int fakultaet(int n){
		int result = 1;
		for(int i = 1; i<=n; i++) {
			result=result*i;
		}
		return result;
	}
}
