
public class Kontotest {
	public static void main (String[]args) {
		Konto k1 = new Konto();
		Konto k2 = new Konto();
		k1.setiban("DE 0000 1199 2938 1234");
		k1.setknr(1);
		k1.setstand(2000000);
		k2.setiban("DE 0000 1299 2938 1234");
		k2.setknr(2);
		k2.setstand(3000000);
		
		System.out.println("1. Iban: "+k1.getiban());
		System.out.println("1. knr: "+k1.getknr());
		System.out.println("1. Kontostand: "+k1.getstand()+"€");
		System.out.println("2. Iban: "+k2.getiban());
		System.out.println("2. knr: "+k2.getknr());
		System.out.println("2. Kontostand: "+k2.getstand()+"€");
		
		k1.abheben(500);
		k1.überweisen(500, k2);
		k1.einzahlen(500);
		k2.getknr();
		k2.getstand();
	}
}

