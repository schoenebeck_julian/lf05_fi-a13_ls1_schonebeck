
public class Konto {
	//Attribute
	private String iban;
	private int knr;
	private double stand;
	//Konstruktor
	public Konto(){};
	//Methoden
	public void setiban(String iban){
		this.iban=iban;
	}
	public void setknr (int knr) {
		this.knr=knr;
	}
	public void setstand (double stand) {
		this.stand=stand;
	}
	public String getiban() {
		return iban;
	}
	public int getknr () {
		return knr;
	}
	public double getstand() {
		return stand;
	}
	public void abheben(double betrag) {
		stand=stand-betrag;
		System.out.println("Ihr Kontostand betr�gt nun "+stand+" Euro");
	}
	public void �berweisen(double betrag, Konto k) {
		stand=stand-betrag;
		k.stand=k.stand+betrag;
	}
	public void einzahlen (double betrag) {
		stand=stand+betrag;
		System.out.println("Ihr Kontostand betr�gt nun "+stand+" Euro");
	}
}