﻿import java.util.Scanner;

class Fahrkartenautomat
{

	public static double fahrkartenbestellungErfassen()
	{ 
		Scanner tastatur = new Scanner(System.in); // Variable: tastatur, Datentyp: Scanner
		double[] fahrkartenpreis=new double[10];
		fahrkartenpreis[0]=2.90;					//Nachteil von diesen Arrays: 
		fahrkartenpreis[1]=3.30;					//Schreibaufwand ist auf den ersten Blick höher.
		fahrkartenpreis[2]=3.60;										
		fahrkartenpreis[3]=1.90;					//Vorteil:
		fahrkartenpreis[4]=8.60;					//Man kann die Tickets dadurch in großer Menge schneller bearbeiten
		fahrkartenpreis[5]=9.00;					//Die Implementierung ist wesentlich übersichtlicher.
		fahrkartenpreis[6]=9.60;
		fahrkartenpreis[7]=2.50;
		fahrkartenpreis[8]=24.30;
		fahrkartenpreis[9]=24.90;
		String[] fahrkartenname=new String[10];
		fahrkartenname[0]=" Einzelfahrschein Berlin AB (1)";
		fahrkartenname[1]=" Einzelfahrschein Berlin BC (2)";
		fahrkartenname[2]=" Einzelfahrschein Berlin ABC (3)";
		fahrkartenname[3]=" Kurzstrecke (4)";
		fahrkartenname[4]=" Tageskarte Berlin AB(5)";
		fahrkartenname[5]=" Tageskarte Berlin BC (6)";
		fahrkartenname[6]=" Tageskarte Berlin ABC (7)";
		fahrkartenname[7]=" Kleingruppen-Tageskarte Berlin AB (8)";
		fahrkartenname[8]=" Kleingruppen-Tageskarte Berlin BC (9)";
		fahrkartenname[9]=" Kleingruppen-Tageskarte Berlin ABC (10)";
	    double zuZahlenderBetrag = 0;
		System.out.println("Wählen Sie bitte Ihr Ticket:");
	    for(int i=0;i<fahrkartenname.length;i++) {
			System.out.println(fahrkartenname[i]+" "+fahrkartenpreis[i]);
		}
	    int auswahl=tastatur.nextInt();
		
		if (auswahl==1){
			zuZahlenderBetrag=fahrkartenpreis[0];
			System.out.print("Ihre Wahl:"+fahrkartenname[0]+"\n");
			int anzahl; // Variable: anzahl, Datentyp: double
		    System.out.print("Anzahl der Tickets: ");
		    anzahl = tastatur.nextInt();
		    zuZahlenderBetrag *= anzahl;
		}
		else if (auswahl==2) {
			zuZahlenderBetrag=fahrkartenpreis[1];
			System.out.print("Ihre Wahl:"+fahrkartenname[1]+"\n");
			int anzahl; // Variable: anzahl, Datentyp: double
		    System.out.print("Anzahl der Tickets: ");
		    anzahl = tastatur.nextInt();
		    zuZahlenderBetrag *= anzahl;
		}
		else if (auswahl==3) {
			zuZahlenderBetrag=fahrkartenpreis[2];
			System.out.print("Ihre Wahl:"+fahrkartenname[2]+"\n");
			int anzahl; // Variable: anzahl, Datentyp: double
		    System.out.print("Anzahl der Tickets: ");
		    anzahl = tastatur.nextInt();
		    zuZahlenderBetrag *= anzahl;
		}
		else if (auswahl==4) {
			zuZahlenderBetrag=fahrkartenpreis[3];
			System.out.print("Ihre Wahl:"+fahrkartenname[3]+"\n");
			int anzahl; // Variable: anzahl, Datentyp: double
		    System.out.print("Anzahl der Tickets: ");
		    anzahl = tastatur.nextInt();
		    zuZahlenderBetrag *= anzahl;
		}
		else if (auswahl==5) {
			zuZahlenderBetrag=fahrkartenpreis[4];
			System.out.print("Ihre Wahl:"+fahrkartenname[4]+"\n");
			int anzahl; // Variable: anzahl, Datentyp: double
		    System.out.print("Anzahl der Tickets: ");
		    anzahl = tastatur.nextInt();
		    zuZahlenderBetrag *= anzahl;
		}
		else if (auswahl==6) {
			zuZahlenderBetrag=fahrkartenpreis[5];
			System.out.print("Ihre Wahl:"+fahrkartenname[5]+"\n");
			int anzahl; // Variable: anzahl, Datentyp: double
		    System.out.print("Anzahl der Tickets: ");
		    anzahl = tastatur.nextInt();
		    zuZahlenderBetrag *= anzahl;
		}
		else if (auswahl==7) {
			zuZahlenderBetrag=fahrkartenpreis[6];
			System.out.print("Ihre Wahl:"+fahrkartenname[6]+"\n");
			int anzahl; // Variable: anzahl, Datentyp: double
		    System.out.print("Anzahl der Tickets: ");
		    anzahl = tastatur.nextInt();
		    zuZahlenderBetrag *= anzahl;
		}
		else if (auswahl==8) {
			zuZahlenderBetrag=fahrkartenpreis[7];
			System.out.print("Ihre Wahl:"+fahrkartenname[7]+"\n");
			int anzahl; // Variable: anzahl, Datentyp: double
		    System.out.print("Anzahl der Tickets: ");
		    anzahl = tastatur.nextInt();
		    zuZahlenderBetrag *= anzahl;
		}
		else if (auswahl==9) {
			zuZahlenderBetrag=fahrkartenpreis[8];
			System.out.print("Ihre Wahl:"+fahrkartenname[8]+"\n");
			int anzahl; // Variable: anzahl, Datentyp: double
		    System.out.print("Anzahl der Tickets: ");
		    anzahl = tastatur.nextInt();
		    zuZahlenderBetrag *= anzahl;
		}
		else if (auswahl==10) {
			zuZahlenderBetrag=fahrkartenpreis[9];
			System.out.print("Ihre Wahl:"+fahrkartenname[9]+"\n");
			int anzahl; // Variable: anzahl, Datentyp: double
		    System.out.print("Anzahl der Tickets: ");
		    anzahl = tastatur.nextInt();
		    zuZahlenderBetrag *= anzahl;
		}
		else {
			System.out.print(">>falsche Eingabe<<\n");
		}
	    
	    return zuZahlenderBetrag;
		
	}
	
	public static double fahrkartenBezahlen(double zuZahlen)
	{
		double eingezahlterGesamtbetrag; // Variable: eingezahlterGesamtbetrag, Datentyp: double
		double eingeworfeneMünze; // Variable: eingeworfeneMünze, Datentyp: double
		double rückgabebetrag; // Variable: rückgabebetrag, Datentyp: double
		Scanner tastatur = new Scanner(System.in); // Variable: tastatur, Datentyp: Scanner

	    
	    eingezahlterGesamtbetrag = 0.0;
	    while(eingezahlterGesamtbetrag < zuZahlen)
	    {
	    	System.out.printf("Noch zu zahlen: %.2f Euro\n",(zuZahlen - eingezahlterGesamtbetrag)); // Differenz von zuZahlenderBetrag und eingezahlterGesamtbetrag wird gebildet
	    	System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	eingeworfeneMünze = tastatur.nextDouble();
	        eingezahlterGesamtbetrag += eingeworfeneMünze; // eingeworfeneMünze wird auf eingezahlterGesamtbetrag addiert
	    }
		return eingezahlterGesamtbetrag - zuZahlen;
		
		
	}
	
	public static void fahrkartenAusgaben()
	{
	    System.out.println("\nFahrscheine wird ausgegeben");
	       
	    for (int i = 0; i < 8; i++)
	    {
	       System.out.print("=");
	       try {
	    	   Thread.sleep(250);
	       } catch (InterruptedException e) {
	    	   // TODO Auto-generated catch block
	    	   e.printStackTrace();
	       }
	    }
	    System.out.println("\n\n");
	}
	
	public static void rueckgeldAusgeben(double rückgabebetrag)
	{
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n",rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0; // Von rückgabebetrag wird 2 subtrahiert
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0; // Von rückgabebetrag wird 1 subtrahiert
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5; // Von rückgabebetrag wird 0,5 subtrahiert
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2; // Von rückgabebetrag wird 0,2 subtrahiert
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1; // Von rückgabebetrag wird 0,1 subtrahiert
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05; // Von rückgabebetrag wird 0,05 subtrahiert
	           }
	           System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
	                   "vor Fahrtantritt entwerten zu lassen!\n"+
	                   "Wir wünschen Ihnen eine gute Fahrt.");
	       }
	       
	       
	       
	       
	    
	       
		
		
	}
	
	
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	char abfrage='j';
		while (abfrage!='n') {
    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	double rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	if (rueckgabebetrag!=0.0){
    		fahrkartenAusgaben();
    	}
    	rueckgeldAusgeben(rueckgabebetrag);
    	System.out.println("Wenn Sie eine Fahrkarte ziehen wollen, dann drücken Sie bitte j, ansonsten n.");
    	abfrage=tastatur.next().charAt(0);
	    

		}
    }
}