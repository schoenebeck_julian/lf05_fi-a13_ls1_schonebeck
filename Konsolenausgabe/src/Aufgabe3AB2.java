
public class Aufgabe3AB2 {
	public static void main(String[] args) {
		System.out.printf("%-12s"+"|"+"%10s\n", "Fahrenheit", "Celsius");
		System.out.println("-----------------------");
		System.out.printf("%-12s"+"|"+"%10s"+"%.2f\n", -20,-28.8889);
		System.out.printf("%-12s"+"|"+"%10s"+"%.2f\n", -10,-28.8889);
		System.out.printf("%-12s"+"|"+"%10s"+"%.2f\n", "+0",-28.8889);
		System.out.printf("%-12s"+"|"+"%10s"+"%.2f\n", "+20",-28.8889);
		System.out.printf("%-12s"+"|"+"%10s"+"%.2f\n", "+30",-28.8889);
	}    
}