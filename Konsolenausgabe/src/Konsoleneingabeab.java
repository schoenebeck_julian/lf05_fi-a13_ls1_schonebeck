import java.util.Scanner; // Import der Klasse Scanner 
 
public class Konsoleneingabeab  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    int zahl1 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    int zahl2 = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    int ergebnis = zahl1 + zahl2; 
    int subergebnis = zahl1 - zahl2;
    int multergebnis = zahl1 * zahl2;
    int divtergebnis = zahl1 / zahl2;
     
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);   
    System.out.print("\n\n\n\nErgebnis der Subtraktion lautet: "); 
    System.out.print(zahl1 + " - " + zahl2 + " = " + subergebnis); 
    System.out.print("\n\n\n\nErgebnis der Multiplikation lautet: "); 
    System.out.print(zahl1 + " x " + zahl2 + " = " + multergebnis);
    System.out.print("\n\n\n\nErgebnis der Division lautet: "); 
    System.out.print(zahl1 + " / " + zahl2 + " = " + divtergebnis);
    
    
    
 
    myScanner.close(); 
     
  }
}
